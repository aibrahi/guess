#This program allows a user to guess a number between 1 and 100

import random

print ("Enter 't' to reveal the number")
print ("To exit the program, enter 'q'")

guesses = 0
number = random.randint (1,100)

guess = raw_input ("Guess a number between 1 and 100: ")

while number != guess:

	guesses = guesses + 1
	
	if guess == 'q':
		exit()
			
	if guess == 't':
		print ("The correct number is %s" % number)
		exit()

	guess = int(guess)
	
	if (guess > number):
		print ("Lower.")
		guess = raw_input("Guess again: ")
	
	if guess < number:
		print ("Higher.")
		guess = raw_input("Guess again: ")
	
	if guess == number:
		print ("Correct! It took you %s tries to guess the number."  % guesses)
	
		

